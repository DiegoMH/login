import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Usuario } from '../models/usuario.model';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class LoginFirebaseService {

  constructor(private http: HttpClient) {
    console.log(this.obtenerToken());
  }

  url: string = 'https://identitytoolkit.googleapis.com/v1/accounts:';
  apiKey = 'AIzaSyCQPWCSDgoxky4KYw-YTlTDQKPz__modF4';
  usuarioToken: string;

  login(usuario: Usuario) {
    const data = {
      email: usuario.email,
      password: usuario.password,
      returnSecureToken: true
    }
    return this.http.post(`${this.url}signInWithPassword?key=${this.apiKey}`, data)
      .pipe(
        map(resp => {
          this.guardarToken(resp['idToken']);
          return resp;
        })
      );
  }

  logout() {
    localStorage.removeItem('idToken');
  }

  registrarUsuario(usuario: Usuario) {
    const data = {
      email: usuario.email,
      password: usuario.password,
      returnSecureToken: true
    }
    return this.http.post(`${this.url}signUp?key=${this.apiKey}`, data)
      .pipe(
        map(resp => {
          this.guardarToken(resp['idToken']);
          return resp;
        })
      );
  }

  guardarToken(idToken: string) {
    this.usuarioToken = idToken;
    localStorage.setItem('idToken', idToken);
    let hoy = new Date();
    hoy.setSeconds(3600);
    localStorage.setItem('expiracion', hoy.getTime().toString());
  }

  obtenerToken() {
    if (localStorage.getItem('idToken'))
      this.usuarioToken = localStorage.getItem('idToken');
    else
      this.usuarioToken = '';

    return this.usuarioToken;
  }

  estaAutenticado(): boolean {
    if (this.usuarioToken.length < 2) {
      return false;
    }

    const expiracion = Number(localStorage.getItem('expiracion'));
    let expiracionDate = new Date();
    expiracionDate.setTime(expiracion);

    if (expiracionDate > new Date()) {
      return true;
    } else {
      return false;
    }
  }

}
