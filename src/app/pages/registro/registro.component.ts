import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { LoginFirebaseService } from 'src/app/services/login-firebase.service';
import Swal from 'sweetalert2';
import { Usuario } from '../../models/usuario.model';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  usuario: Usuario;

  constructor(private service: LoginFirebaseService) { }

  ngOnInit() {
    this.usuario = new Usuario();
  }

  registrarUsuario(form: NgForm) {
    if(!form.valid) { return; }
    Swal.fire({
      allowOutsideClick: false,
      icon: 'info',
      text: 'Espere por favor...'
    });
    Swal.showLoading();
    this.service.registrarUsuario(this.usuario)
      .subscribe(resp => {
        Swal.close();
      }, err => {
        Swal.fire({
          icon: 'error',
          title: 'Error al registrar',
          text: err.error.error.message
        });
        console.log(err.error.error.message);
      });

  }


}
