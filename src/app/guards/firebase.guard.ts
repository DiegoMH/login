import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { LoginFirebaseService } from '../services/login-firebase.service';

@Injectable({
  providedIn: 'root'
})
export class FirebaseGuard implements CanActivate {

  constructor(private service: LoginFirebaseService, private router: Router) { }

  canActivate(): boolean {
    if (this.service.estaAutenticado()) {
      return true;
    } else {
      this.router.navigateByUrl('/registro');
      return false;
    }
  }

}
